#ifndef DRON_HH
#define DRON_HH

#include "Prostopadloscian.hh"


class Dron
{
    Prostopadloscian *korpus;
  public:
Dron();

  void Sterowanie(double kat, double kat_wznoszenia, double droga);
};

  Dron::Dron()
  {
    
      korpus = new Prostopadloscian(10,10,10,"bryly/korpus.dat","bryly/korpus_zap.dat");
      korpus->zapis();
      korpus->zapis_org();
  }

  void Dron::Sterowanie(double kat, double kat_wznoszenia, double droga)
  {
      korpus->set_kat_obrotu(kat);
      korpus->set_kat_wznoszenia(kat_wznoszenia);
      korpus->set_przesuniecie(droga);
      korpus->translacja();
      korpus->zapis();
  }

#endif