#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "rozmiar.hh"
#include "Wektor.hh"
#include <cmath>
#include <cstdlib>
#include <iostream>

using std::abs;
const double f=0.00000001;

template <typename T, int ROZMIAR>
class Macierz {

    T macierz[ROZMIAR][ROZMIAR];
/*!
* Realizuje zamienianie wierszy macierzy jesli na przekatnej znajduje sie 0
* Argumenty:
*    macierz - macierz, ktora ma byc zmieniona
*    znak-   zmienna potrzebna do zmiany znaku wyznacznika
*/
    void ZeraNaPrzekatnej(int &znak); 
/* !
* Realizuje zerowanie wierszy macierzy metoda eliminacji Gaussa
* Argumenty:
*    det-  wyznacznik macierzy
*
*/
    void ZerowanieWierszy(T &det); 
public:

/*!
 * Przeciazenia operatora fukcyjnego dla macierzy
 * Argumenty:
 * wiersz -ilosc wierszy
 * kolumna- ilosc kolumn
 * Zwraca:
 * Macierz o danym rozmiarze
 * lub w przypadku zlego rozmiaru komunikat o bledzie
 */
    const T &operator()(unsigned int wiersz, unsigned int kolumna) const;

    T &operator()(unsigned int wiersz, unsigned int kolumna);
/*!
* Realizuje transpozycje macierzy
*/
    void Transponowanie();
/*!
* Realizuje wyliczenie wyznacznika macierzy
* Argumenty:
*    macierz - macierz, ktorej wyznacznik ma zostac policzony
* Zwraca:
*    det- policzony wyznacznik
*/  
    T WyznacznikMacierzy(Macierz<T, ROZMIAR> macierz) const; 
 /*!
 * Mnozenie macierzy przez wektor
 * Argumenty:
 * vec- wektor, przez ktory mamy pomnozyc macierz
 * Zwraca:
 * Wynik dzialania
 */
    Wektor<T, ROZMIAR> operator * (Wektor<T, ROZMIAR> wektor) const; 
/*!                                                                                                                                                                              
 * Konstruktor dla klasy Macierz                                                                                                                                                 
 * Inicjuje pola klasy poczatkowymi wartosciami                                                                                                                                  
 */
    Macierz();

};

/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wczytywania macierzy                                                                                                                     
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wejscia                                                                                                                                                     
 *    Mac - wektor, ktory funkcja ma wczytac,                                                                                                                                    
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu istream                                                                                                                                            
 */
        template <typename T, int ROZMIAR>
        std::istream& operator >> (std::istream &Strm, Macierz<T, ROZMIAR> &Mac);

/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wyswietlania macierzy                                                                                                                    
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wyjscia                                                                                                                                                     
 *    Mac - wektor, ktory funkcja ma wczytac,                                                                                                                                    
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu ostream                                                                                                                                            
 */               
        template <typename T, int ROZMIAR>
        std::ostream& operator << (std::ostream &Strm, const Macierz<T, ROZMIAR> &Mac);

template <typename T, int ROZMIAR>
Macierz<T, ROZMIAR>::Macierz() {    //konstruktor
    for (int i=0; i<ROZMIAR; i++)
    {
        for (int j=0; j<ROZMIAR; j++)
        {
            macierz[i][j]=0;
        }
    }
}

template <typename T, int ROZMIAR>
const T &Macierz<T, ROZMIAR>::operator()(unsigned int wiersz, unsigned int kolumna) const {     //operator funkcyjny
    if (wiersz < 0 || wiersz >= ROZMIAR || kolumna < 0 || kolumna >= ROZMIAR) {
        std::clog << "Indeks tablicy wyszedl poza zakres" << std::endl;
        exit(1);
    }
    return macierz[wiersz][kolumna];
}

template <typename T, int ROZMIAR>
T &Macierz<T, ROZMIAR>::operator()(unsigned int wiersz, unsigned int kolumna) {     //operator funkcyjny

    return const_cast<T &>(const_cast<const Macierz<T, ROZMIAR> *>(this)->operator()(wiersz, kolumna));
}

template <typename T, int ROZMIAR>
std::ostream& operator << (std::ostream &Strm, const Macierz<T, ROZMIAR> &Mac){  //Wyswietlanie wektora 

    for (int i=0; i<ROZMIAR; i++)
    {
        for (int j=0; j<ROZMIAR; j++)
        {
            Strm<<Mac(i,j)<<" ";
        }
        Strm<<std::endl;
    }
    return Strm;
}
template <typename T, int ROZMIAR>
std::istream& operator >> (std::istream &Strm, Macierz<T, ROZMIAR> &Mac){    //Wczytywanie wektora

    for (int i=0; i<ROZMIAR; i++)
    {
        for (int j=0; j<ROZMIAR; j++)
        {
            Strm>>Mac(i,j);
        }
    }
    return Strm;
}

template <typename T, int ROZMIAR>
Wektor<T, ROZMIAR> Macierz<T, ROZMIAR>::operator*(Wektor<T, ROZMIAR> wektor) const {    //mnozenie macierzy razy wektor
   Wektor<T, ROZMIAR> weektor;
    for (int i = 0; i < ROZMIAR; i++) {
        for (int j = 0; j < ROZMIAR; j++) {
            weektor[i] = weektor[i] + macierz[j][i]*wektor[j];
        }
    }
    return weektor;
}

template <typename T, int ROZMIAR>
void Macierz<T, ROZMIAR>::ZeraNaPrzekatnej(int &znak) { //zamienianie wierszy macierzy jesli na przekatnej znajduje sie 0
    //sprawdzenie czy na przekatnej znajduje sie 0
    for (int i = 0; i < ROZMIAR; i++) {
        if (abs(macierz[i][i]) < f) { //jesli tak zamiana wierszy
            for (int a = i; a < ROZMIAR; a++) { //szukamy wiersza z ktorym mozemy zamienic
                if (abs(macierz[a][i]) > f) {
                    znak = (-1)*znak;
                    for (int j = 0; j < ROZMIAR; j++) {
                        swap(macierz[i][j], macierz[a][j]);
                    }
                }
            }
        }
    }
}

template <typename T, int ROZMIAR>
void Macierz<T, ROZMIAR>::ZerowanieWierszy(T &det) {   //zerowanie wierszy macierzy metoda eliminacji Gaussa
    T wsp; //zmienna potrzebna przy zerowaniu wierszy
    for (int a = 0; a < ROZMIAR-1; a++) { //dla liczenia stalej wsp=macierz[i][a]/macierz[a][a]
        if (abs(macierz[a][a]) > f) { ///sprawdzenie aby uniknac dzielenia przez 0
            for (int i = a + 1; i < ROZMIAR; i++) { //porusza sie po wierszach w kolumnie do wyzerowania
                wsp = macierz[i][a] / macierz[a][a]; //obliczenie wartosci wspolczynnika do pomnozenia calego wiersza
                for (int j = 0; j < ROZMIAR; j++) { //pomnozenie przez stala  
                    macierz[i][j] = macierz[i][j] - macierz[a][j] * wsp;
                }
            }
        } else det = macierz[a][a];
    }
}

template <typename T, int ROZMIAR>
T Macierz<T, ROZMIAR>::WyznacznikMacierzy(Macierz<T, ROZMIAR> macierz) const { //wyliczanie wyznacznika
    T det;
    int znak = 1; //zmienna potrzebna do zmiany znaku jesli zamienimy wiersze
    macierz.ZeraNaPrzekatnej(znak); //sprawdzenie czy na przekatnej nie ma zera
    macierz.ZerowanieWierszy(det); //zerowanie wierszy
    for (int i=0 ; i < ROZMIAR; i++){
        if (i == 0){
            det=macierz(i, i);
        } 
        else{
            det=macierz(i,i)*det;
        }
    }
    det=det*znak;
    return det;
}

template <typename T, int ROZMIAR>
void Macierz<T, ROZMIAR>::Transponowanie() {     //transpozycja macierzy

    for (int i = 0; i < ROZMIAR; i++) {
        for (int j = i + 1; j < ROZMIAR; j++) {
            swap(macierz[i][j], macierz[j][i]);
        }
    }
}
#endif
