#ifndef POWIERZCHNIA_WODY_HH
#define POWIERZCHNIA_WODY_HH


#include "Plaszczyzna.hh"

class Powierzchnia_Wody : public Plaszczyzna
{
public:
    Powierzchnia_Wody(int rozmiar_Dna, int glebokosc_dna, int podzial, int wysokosc_fali, string nazwa_oryginalu, string nazwa_do_wypisania);
};

    Powierzchnia_Wody::Powierzchnia_Wody(int rozmiar_Dna, int glebokosc_dna, int podzial, int wysokosc_fali, string nazwa_oryginalu, string nazwa_do_wypisania)
    {
        Wektor3D punkt;
        gestosc_siatki = 0;
        this->nazwa_oryginalu = nazwa_oryginalu;
        this->nazwa_do_wypisania = nazwa_do_wypisania;

        for (int szerokosc = -rozmiar_Dna/2; szerokosc <= rozmiar_Dna/2; szerokosc += podzial)
        {
            gestosc_siatki++;
            for (int dlugosc = -rozmiar_Dna/2; dlugosc <= rozmiar_Dna/2; dlugosc += podzial)
            {
                punkt[0] = szerokosc;
                punkt[1] = dlugosc;
                if(gestosc_siatki%2==0)
                punkt[2] = glebokosc_dna;
                else
                punkt[2] = glebokosc_dna + wysokosc_fali;
                
                punkty.push_back(punkt);
            }
        }
    } 

#endif