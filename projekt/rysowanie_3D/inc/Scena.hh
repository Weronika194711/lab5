#include <iostream>
#include <iomanip>
#include "lacze_do_gnuplota.hh"
#include "Powierzchnia_Dna.hh"
#include "Powierzchnia_Wody.hh"
#include "Dron.hh"

class Scena
{
    Powierzchnia_Dna *dno;
    Powierzchnia_Wody *woda;
    Dron dron;
    PzG::LaczeDoGNUPlota  &Lacze;
    public:
    Scena( PzG::LaczeDoGNUPlota  &Lacze);
    void ruch(double wznoszenie, double obrot, double droga);

};
    void Scena::ruch(double obrot,double wznoszenie,  double droga)
    {
        dron.Sterowanie(obrot,wznoszenie,droga);
    }
    
    Scena::Scena( PzG::LaczeDoGNUPlota  &Lacze):Lacze(Lacze)
    {
        
        dno = new Powierzchnia_Dna(100,-30,10,"bryly/dno.dat","bryly/dno_zap.dat");
        woda = new Powierzchnia_Wody(100,30,10,10,"bryly/woda.dat","bryly/woda_zap.dat");    
        dno->zapis();
        dno->zapis_org();
        woda->zapis();
        woda->zapis_org();

        Lacze.DodajNazwePliku("bryly/dno_zap.dat");
        Lacze.DodajNazwePliku("bryly/woda_zap.dat");
        Lacze.DodajNazwePliku("bryly/korpus_zap.dat");

    }