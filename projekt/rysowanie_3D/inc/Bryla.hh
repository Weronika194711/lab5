#ifndef BRYLA_HH
#define BRYLA_HH
#include "PrzestrzenW3D.hh"

class Bryla : public PrzestrzenW3D
{
protected:
    int ilosc_wierzcholkow;

public:
    void zapis();
    void zapis_org();
};

    void Bryla::zapis()
    {
        
        ofstream plik;
        plik.open(nazwa_do_wypisania);

        for (int i = 0; i < ilosc_wierzcholkow; i++)
        {
            if (i % 2 == 0)
                plik << endl;

            plik << punkty[i] << endl;
    
        }
        plik.close();
    }
    
    void Bryla::zapis_org()
    {
        ofstream plik;
        plik.open(nazwa_oryginalu);

        for (int i = 0; i < ilosc_wierzcholkow; i++)
        {
            if (i % 2 == 0)
                plik << endl;

            plik << punkty[i] << endl;
    
        }
        plik.close();
    }

#endif