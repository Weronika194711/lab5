#ifndef MACIERZ3X3_HH
#define MACIERZ3X3_HH


#include "Macierz.hh"

using namespace std; 

typedef Macierz<double,3> Macierz3x3;

void osZ(Macierz3x3 &M,double kat)
{
M(0,0)=cos(kat);
M(0,1)=-sin(kat);
M(1,0)=sin(kat);
M(1,1)=cos(kat);
M(2,2)=1;
}
void osY(Macierz3x3 &M,double kat)
{
M(0,0)=cos(kat);
M(0,2)=sin(kat);
M(1,1)=1;
M(2,0)=-sin(kat);  
M(2,2)=cos(kat);  
}

#endif