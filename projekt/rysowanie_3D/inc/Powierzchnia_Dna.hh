#ifndef POWIERZCHNIA_DNA_HH
#define POWIERZCHNIA_DNA_HH

#include "Plaszczyzna.hh"

class Powierzchnia_Dna : public Plaszczyzna
{
public:
    Powierzchnia_Dna(int rozmiar_Dna, int glebokosc_dna, int podzial, string nazwa_oryginalu, string nazwa_do_wypisania);
};

    Powierzchnia_Dna::Powierzchnia_Dna(int rozmiar_Dna, int glebokosc_dna, int podzial, string nazwa_oryginalu, string nazwa_do_wypisania)
    {
        Wektor3D punkt;
        gestosc_siatki = 0;
        this->nazwa_oryginalu=nazwa_oryginalu;
        this->nazwa_do_wypisania=nazwa_do_wypisania;

        for (int szerokosc = -rozmiar_Dna/2; szerokosc <= rozmiar_Dna/2; szerokosc += podzial)
        {
            gestosc_siatki++;
            for (int dlugosc = -rozmiar_Dna/2; dlugosc <= rozmiar_Dna/2; dlugosc += podzial)
            {
                punkt[0] = szerokosc;
                punkt[1] = dlugosc;
                punkt[2] = glebokosc_dna;
                punkty.push_back(punkt);
            }
        }
    }

#endif