#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include "Bryla.hh"

class Prostopadloscian : public Bryla
{
public:
    Prostopadloscian(double dlugosc, double szerokosc, double wysokosc, string nazwa_oryginalu, string nazwa_do_wypisania)
    {
        
        Wektor3D punkt;
        ilosc_wierzcholkow = 10;
        punkty.resize(ilosc_wierzcholkow);
        this->nazwa_oryginalu = nazwa_oryginalu;
        this->nazwa_do_wypisania = nazwa_do_wypisania;
        for (int i = 0; i < 10; i++)
        {
            punkty[i][0] = -dlugosc / 2;
            punkty[i][1] = -szerokosc / 2;
            punkty[i][2] = -wysokosc / 2;
        }
        punkty[1][0] += dlugosc;

        punkty[2][1] += szerokosc;

        punkty[3][0] += dlugosc;
        punkty[3][1] += szerokosc;

        punkty[4][1] += szerokosc;
        punkty[4][2] += wysokosc;

        punkty[5][0] += dlugosc;
        punkty[5][1] += szerokosc;
        punkty[5][2] += wysokosc;

        punkty[6][2] += wysokosc;

        punkty[7][0] += dlugosc;
        punkty[7][2] += wysokosc;

        punkty[9][0] += dlugosc;
    }
};

#endif