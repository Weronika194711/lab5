#ifndef PRZESTRZENW3D_HH
#define PRZESTRZENW3D_HH

#include "Wektor3D.h"
#include "Macierz3x3.h"
#include <vector>
#include <cmath>
#include <fstream>

class PrzestrzenW3D
{
    protected:
    vector<Wektor3D> punkty;
    Wektor3D Translacja;
    Macierz3x3 obrot;
    Macierz3x3 Wznoszenie;
    double kat_obrotu;
    double kat_wznoszenia;
    string nazwa_oryginalu;
    string nazwa_do_wypisania;
    public:
   
    void set_kat_obrotu(double kat);
    void set_przesuniecie(double droga);
    void set_kat_wznoszenia(double kat);
    void translacja();

};

    void PrzestrzenW3D::set_kat_obrotu(double kat)
    {
        kat_obrotu+=kat*M_PI/180;
    }
    void PrzestrzenW3D::set_przesuniecie(double droga)
    {
        Wektor3D pom;
        pom[0]=droga;
        osY(Wznoszenie,kat_wznoszenia);
        osZ(obrot,kat_obrotu);
        Translacja=Translacja+(Wznoszenie*(obrot*pom));
    }
    void PrzestrzenW3D::set_kat_wznoszenia(double kat)
    {
        kat_wznoszenia=kat*M_PI/180;
    }
    void PrzestrzenW3D::translacja()
    {
        ifstream plik;
        plik.open(nazwa_oryginalu);
             for(int i=0; i<(int)punkty.size(); i++)
            {
                plik>>punkty[i];
            }
        plik.close();
            for(int i=0; i<(int)punkty.size(); i++)
            {
                punkty[i]=obrot*punkty[i]+Translacja;
            }
    }

#endif