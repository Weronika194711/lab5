#ifndef PLASZCZYZNA_HH
#define PLASZCZYZNA_HH
#include "PrzestrzenW3D.hh"


class Plaszczyzna : public PrzestrzenW3D
{
    protected:
    int gestosc_siatki;
public:

void zapis();
void zapis_org();
};

    void Plaszczyzna::zapis()
    {
        ofstream plik;
        plik.open(nazwa_do_wypisania);
        int k=0;
        for(int i=0; i<(int)punkty.size()/gestosc_siatki; i++)
        {
            for(int j=0; j<gestosc_siatki; j++)
            {
                plik<<punkty[k]<<endl;
                k++;
            }
                plik<<endl;
        }
        plik.close();
    }

    void Plaszczyzna::zapis_org()
    {
        ofstream plik;
        plik.open(nazwa_oryginalu);
        int k=0;
        for(int i=0; i<(int)punkty.size()/gestosc_siatki; i++)
        {
            for(int j=0; j<gestosc_siatki; j++)
            {
                plik<<punkty[k]<<endl;
                k++;
            }
                plik<<endl;
        }
        plik.close();  
    }
#endif