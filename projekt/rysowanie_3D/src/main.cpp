#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "lacze_do_gnuplota.hh"

using namespace std;
#include "Scena.hh"

int main()
{

  PzG::LaczeDoGNUPlota Lacze;
  Scena scena(Lacze);
  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.Inicjalizuj(); // Tutaj startuje gnuplot.

  Lacze.UstawZakresX(-60, 50);
  Lacze.UstawZakresY(-60, 50);
  Lacze.UstawZakresZ(-60, 50);

  Lacze.UstawRotacjeXZ(80, 60); // Tutaj ustawiany jest widok

  Lacze.Rysuj(); // Teraz powinno pojawic sie okienko gnuplota
                 // z rysunkiem, o ile istnieje plik "prostopadloscian1.dat"
  char symbol;
  double droga, kat, wznoszenie;
  cout << "r-ruch, o-obrot, m-menu, k-koniec" << endl;
  while (1)
  {
    cout<<"podaj opcje"<<endl;
    cin >> symbol;

    switch (symbol)
    {

    case 'r':
      cout << "podaj kat wznoszenia : ";
      cin >> wznoszenie;

      cout << "podaj dorge : ";
      cin >> droga;

      if (droga >= 0)
      {
        for (double i = 0; i < droga; i++)
        {
          scena.ruch(0, wznoszenie, 1);
          Lacze.Rysuj();
          usleep(200000);
        }
      }
      else
      {
        cerr << "droga nie moze byc ujemna" << endl;
      }

      break;

    case 'o':
      cout << "podaj kat : ";
      cin >> kat;

      if (kat >= 0)
      {
        for (double i = 0; i < kat; i++)
        {
          scena.ruch(1, 0, 0);
          Lacze.Rysuj();
          usleep(200000);
        }
      }
      else
      {
        for (double i = 0; i < (-kat); i++)
        {
          scena.ruch(-1, 0, 0);
          Lacze.Rysuj();
          usleep(200000);
        }
      }
      break;

    case 'm':
      cout << "r-ruch, o-obrot, m-menu, k-koniec" << endl;
      break;
      
    case 'k':
      return 0;
      break;
    }
  }
}
